//
//  ahash.cpp
//  MoreTEsting
//
//  Created by Brian Flores & Jonix on 12/3/15.
//  Copyright © 2015 Brian Flores. All rights reserved.
//

#include "ahash.hpp"
#include <stdio.h>
#include<iostream>
#include<vector>
#include<string>


using namespace std;


void uims::add( const string uid ){
    ++queryCount;
    size_t index = hashValue( uid );
    if( table[index].next == NULL )
    {
        table[index].userID = uid;
        table[index].customerID = queryCount;
        table[index].next = new node;
        table[index].next->next = NULL;
    }
    else{
        node * p = table[index].next;
        while( p->next != NULL )
            p = p->next;
        p->userID = uid;
        p->customerID = (int)queryCount;
        p->next = new node;
        p->next->next = NULL;
    }
}

void uims::erase( const string uid ){ // Not needed
    size_t index = hashValue( uid );
    if( table[index].next != NULL ){
        if( table[index].userID == uid ){
            delete table[index].next;
            table[index].next = NULL;
        }
        else{
            node * p = &table[index];
            while( p->next->userID != uid )
                p = p->next;
            node * pv = p->next;
            p->next = p->next->next;
            delete pv;
        }
    }
}

bool uims::isAvailable(const string uid){
    bool isAvailable = false;
    size_t index = hashValue(uid);
    node *p= &table[index];
    
    while(p->next != NULL)
    {
        if(p->userID == uid){
            isAvailable = true;
        }
        p= p->next;
    }
    return isAvailable;
}

int uims::lookupCustomerId(const string uid){
    size_t index = hashValue(uid);
    int cid;
    node *p = &table[index];
    while(p ->next !=NULL)
    {
        if(p->userID == uid){
            cid = p->customerID;
        }
        p = p->next;
    }
    return cid;
}

void uims::generateSalt(){
    salt = rand() % 100;
    //printf("salt %d\n",salt); //This Is A test
}

int uims::load(){
    int load;
    load = queryCount/Table_Size;
    return load;
}

/*void uims::reallocate(){
    int oldtablesize;
    
    //node *oldtable = table;
    for( size_t i=0; i <Table_Size; i++)
    {
        node *temp = &table[i];
    }
    while(temp !=NULL)
 {
        
    }
}*/

void uims::print()
{   string  out;
    for(int i =0; i < Table_Size; i++)
    {
        if(table[i].next != NULL)
        {
            printf("%d.",i);
            node * p = &table[i];
            while(p ->next != NULL)
            {
                out = p->userID;
                printf("%s,",out.c_str());
                p = p->next;
            }
            printf("\n");
        }
    }
}

size_t uims::hashValue(const std::string uid){
    
    size_t v = 0;
    for( size_t i = 0; i != uid.size(); ++i ){
        v = ( v << 4 ) + uid[i];
        size_t g = v & 0xF0000000;
        if( g != 0 ){
            v = v ^ ( g >> 24 );
            v = v ^ g;
        }
    }
    v = v % Table_Size;
    return v;
}