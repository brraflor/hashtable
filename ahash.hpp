//
//  ahash.hpp
//  MoreTEsting
//
//  Created by Brian Flores & Jonix on 12/3/15.
//  Copyright © 2015 Brian Flores. All rights reserved.
//

#ifndef ahash_hpp
#define ahash_hpp

#include <stdio.h>
#include<iostream>
#include<vector>
#include<string>
#include<fstream>

using namespace std;

const size_t Table_Size = 521;

struct node{
    string userID;
    int customerID;
    node * next;
};


class uims{
public:
    
    uims()
    :queryCount( 0 ){
        for( size_t i = 0; i != Table_Size; ++i )
            table[i].next = NULL;
    }
    void add( const string uid );
    void erase( const string uid );
    
    bool isAvailable(const string uid);
    int lookupCustomerId(const string uid);
    void print();
    void generateSalt();
    int load();
    void reallocate();
    
private:
    size_t hashValue( const string uid );
    node table [Table_Size];       //array
    int queryCount;		//number of customerID
    int salt;           //salt value
};



#endif /* ahash_hpp */
