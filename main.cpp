//Hash table
//Created By Brian Flores & Jonix Ga

#include<iostream>
#include<vector>
#include<string>
#include<stdio.h>

#include "ahash.hpp"

using namespace std;

int main(){
    uims test; 
    char ID_limit[20] = "";
    char invalidSetChar[] = "!@#$%^&*()_+-=";
    char *badChar;
    size_t numOfCharacters = 0;
    
    cout << "Enter Valid User ID: ";
    
    //gets arguements
    cin.getline(ID_limit,20);
    
    numOfCharacters = strlen(ID_limit);
    badChar = strpbrk(ID_limit, invalidSetChar);
    
    if (numOfCharacters <= 16){
        if(badChar > 0)
            cout << "Invalid User ID: Bad Symbol Input." << endl;
        
        else{
            cout << "Valid User ID." << endl;
            if(test.isAvailable(ID_limit) == false)
            {
                test.add(ID_limit);
            }
        }
    }else{
        cout << "User ID exceeds 16 characters." << endl;	
    }

    test.print();
    
    return 0;
}